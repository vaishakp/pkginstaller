##########################
# Set env
##########################

PackName="openmpi"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${Package}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=no
UNPACK=no
COMPILE=no
CREATE_MODULE=yes


PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

##################################
# Download
##################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm ${File}
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://ftp.gnu.org/gnu/${PackName}/"
    base_url="https://download.open-mpi.org/release/open-mpi/v4.1"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

####################################
# Unpack
####################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    tar -xvzf ${File} 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ${Package}
echo ${pwd}

#####################################
# Compile
#####################################

if [ $COMPILE == $"yes" ]; then
    make clean -j
    ./configure CC=clang CXX=clang++ FC=flang \
                CFLAGS="${PKG_CFLAGS}" CXXFLAGS="${PKG_CXXFLAGS}" \
                --with-ucx="/usr" \
		--with-knem="${SOFT_ROOT}/knem-1.1.4" \
		--with-xpmem="${SOFT_ROOT}/xpmem-2.6.5" \
		--with-hwloc=${SOFT_ROOT}/hwloc-2.9.0 \
		--with-hcoll="/opt/mellanox/hcoll" \
		--with-pmi --with-platform=contrib/platform/mellanox/optimized \
		--with-slurm --with-pmi \
                --prefix="${SOFT_ROOT}/${Package}-aocc" 2>&1 | tee ${LogPath}
    make -j all 2>&1 | tee ${LogPath}
    make -j install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


#####################################
# Module
#####################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "${PackVers}-aocc" "${Package}-aocc"
else
    echo "Skipping Module creation stage"
fi

#####################################
# Finalize
#####################################

echo "Target successfully compiled for ${Package}"
cd ${SOURCE_ROOT}
