#!/bin/bash

###############
# GL hack
##############

# Copy the gl.h in required directory


cd $SOFT_ROOT
mkdir -p openGL/include/GL
cp "hwloc-${packages["hwloc"]}/include/hwloc/gl.h openGL/include/GL/"

###############
# Module
###############

create_module "openGL" "0" "openGL"
