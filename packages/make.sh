##################
# Set env
##################

PackName="make"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${Package}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=no
UNPACK=no
COMPILE=no
CREATE_MODULE=yes


PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

###################################
# Download 
###################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $File
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://ftp.gnu.org/gnu/${PackName}"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

##############################
# Unpack
##############################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    tar -xvzf ${File} 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ${Package}
echo ${pwd}

###############################
# Compile
###############################

if [ $COMPILE == $"yes" ]; then
    ./configure CFLAGS="${PKG_CFLAGS}" CXXFLAGS="${PKG_CXXFLAGS}" --prefix=${SOFT_ROOT}/${Package} 2>&1 | tee ${LogPath}
    make -j 2>&1 | tee ${LogPath}
    make install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


#################################
# Module
#################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
else
    echo "Skipping Module creation stage"
fi

#################################
# Finalize
#################################

echo "Target successfully compiled for ${Package}"
cd ${SOURCE_ROOT}
