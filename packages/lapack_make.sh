############
# Set env
############

PackName="lapack"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="v${PackVers}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=no
UNPACK=no
COMPILE=yes
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
#PKG_CFLAGS="-O3 -march=native -mtune=native -mavx2 -mfma"
PKG_CXXFLAGS=$CFLAGS


sed -i "/CFLAGS=/ c\ CFLAGS=${PKG_CFLAGS}" $PKGCOM_HOME/support/make.inc
##################################
# Download
##################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $File
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://github.com/Reference-LAPACK/lapack/archive/refs/tags"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

##################################
# Unpack
##################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    tar -xvzf ${File} 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ${Package}
echo ${pwd}

###################################
# Compile
###################################

if [ $COMPILE == $"yes" ]; then
    mkdir -p ${SOFT_ROOT}/${Package}/lib
    cp ${PKGCOM_HOME}/support/make.inc ${SOFT_ROOT}/source/${Package}/
    sed -i "/CFLAGS=/ c\ CFLAGS=${PKG_CFLAGS}" ${SOFT_ROOT}/source/${Package}/make.inc
    sed -i "/FFLAGS=/ c\ FFLAGS=${PKG_CFLAGS}" ${SOFT_ROOT}/source/${Package}/make.inc

    make all -j
    cp *.a ${SOFT_ROOT}/${Package}/lib
else
    echo "Skipping Configure stage"
fi

###################################
# Module
###################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
else
    echo "Skipping Module creation stage"
fi

###################################
# Finalize
###################################

echo "Target script finished for ${Package}"
cd ${SOURCE_ROOT}
