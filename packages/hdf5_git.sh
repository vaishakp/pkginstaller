######################################
# Set env
######################################

PackName="hdf5"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${Package}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"


DOWNLOAD=no
UNPACK=no
COMPILE=yes
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

########################################
# Download stage
########################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $File
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    #base_url="https://github.com/HDFGroup/hdf5/archive/refs/tags/"
    full_url="git@github.com:HDFGroup/hdf5.git"
    echo $full_url
    git clone ${full_url} ${PackName} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

#######################################
# Fetch
#######################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Fetching repo"
else
    echo "Skipping Unpack stage"
fi

cd ${PackName}
git pull --rebase
git checkout ${Package//-/_}
echo ${pwd}

##########################################
# Compile
##########################################

if [ $COMPILE == $"yes" ]; then
    ./autogen.sh
    make clean
    ./configure CFLAGS="${PKG_CFLAGS}" --enable-optimizations=high --enable-hl --enable-parallel --enable-fortran --prefix="${SOFT_ROOT}/${Package}-parallel" 2>&1 | tee ${LogPath}
    make -j 2>&1 | tee ${LogPath}
    make install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


#########################################
# Module
#########################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "${PackVers}-parallel" "${Package}-parallel"
else
    echo "Skipping Module creation stage"
fi

########################
# Finalize
########################

echo "Target script finished for ${Package}"
cd ${SOURCE_ROOT}
