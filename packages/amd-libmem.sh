######################################
# Set env
######################################

PackName="amd-libmem"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="aocl-libmem-linux-aocc-${PackVers}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"


DOWNLOAD=yes
UNPACK=yes
COMPILE=no
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

########################################
# Download stage
########################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $File
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://download.amd.com/developer/eula/libmem/libmem-${PackVers//./-}"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

#######################################
# Unpack
#######################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpack repo"
    tar -xvzf $File -C $SOFT_ROOT
    mv $SOFT_ROOT/${PackName} $SOFT_ROOT/${Package}

else
    echo "Skipping Unpack stage"
fi

cd $SOFT_ROOT/${Package}
echo ${pwd}

##########################################
# Compile
##########################################

#########################################
# Module
#########################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
    mkdir -p $HOME/privatemodules/amd/${PackName}
    mv $HOME/privatemodules/${PackName}/${PackVers} $HOME/privatemodules/amd/${PackName}/${PackVers}
else
    echo "Skipping Module creation stage"
fi

########################
# Finalize
########################

echo "Target script finished for ${Package}"
cd ${SOURCE_ROOT}
