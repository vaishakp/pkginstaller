###################################################
# Set env
###################################################

PackName="petsc"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${Package}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=no
UNPACK=no
COMPILE=yes
CREATE_MODULE=no

#PKG_CFLAGS=$CFLAGS
PKG_CFLAGS="-O3 -march=native -mtune=native -mavx2 -mfma"
PKG_CXXFLAGS=$CFLAGS

###################################################
# Download
###################################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm ${File}
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://ftp.mcs.anl.gov/pub/${PackName}/release-snapshots"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

###################################################
# Unpack
###################################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    tar -xvzf ${File} 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ${Package}
echo ${pwd}

####################################################
# Compile
####################################################

if [ $COMPILE == $"yes" ]; then
    # Clean
    make clean -j
   
    # Compile
    ./configure \
    --with-mpi-dir="${SOFT_ROOT}/openmpi-4.1.4" \
    --with-hdf5=0 \
    --COPTFLAGS="${PKG_CFLAGS}" --CXXOPTFLAGS="${PKG_CXXFLAGS}" --FOPTFLAGS="${PKG_CFLAGS}" \
    --prefix=$SOFT_ROOT/${Package} 2>&1 | tee ${LogPath}
    
    # Make
    makecmd=$(awk '/make PETSC_DIR/ {print}' ${LogPath})
    echo ${makecmd}
    eval "${makecmd}" 2>&1 | tee ${LogPath}
    
    # Make install
    installcmd=$(awk '/debug install/ {print}' ${LogPath})
    eval "${installcmd}" 2>&1 | tee ${LogPath}
    
    # Check
    checkcmd=$(awk '/check/ {print}' ${LogPath})
    eval "${checkcmd}" 2>&1 | tee ${LogPath}

else
    echo "Skipping Configure stage"
fi

####################################################
# Module
####################################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
else
    echo "Skipping Module creation stage"
fi

####################################################
# Finalize
####################################################

echo "Target successfully compiled for ${Package}. Please make and make install"
cd ${SOURCE_ROOT}
