##########
# Set env
##########

PackName="gcc"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${PackName}-${PackVers}.tar.xz"
LogFile="${PackName}-${PackVers}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=no
UNPACK=no
COMPILE=yes
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

##################
# Download stage
##################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm ${File}
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi

if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    #base_url="https://ftp.mpi-inf.mpg.de/mirrors/gnu/mirror/gcc.gnu.org/pub/gcc/releases/"
    #base_url="http://ftp.tsukuba.wide.ad.jp/software/gcc/releases/"
    #base_url="https://ftp.fu-berlin.de/unix/languages/gcc/releases/"
    #base_url="https://ftp.nluug.nl/languages/gcc/releases/"
    base_url="http://mirror.koddos.net/gcc/releases/"
    full_url="${base_url}/${Package}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

#####################
# Unpack stage
#####################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    rm -rf ${Package}
    tar -xvf ${File} 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ${Package}
echo ${pwd}

#########################
# Compile stage 
#########################

if [ $COMPILE == $"yes" ]; then
    ./contrib/download_prerequisites 2>&1 | tee ${LogPath}
    rm -rf gcc-build
    mkdir -p gcc-build
    cd gcc-build
    #make clean -j
    ../configure CFLAGS="${PKG_CFLAGS}" CXXFLAGS="${PKG_CXXFLAGS}" --enable-bootstrap --disable-multilib --enable-default-pie --prefix=${SOFT_ROOT}/${Package} 2>&1 | tee ${LogPath}
    make -j 2>&1 | tee ${LogPath}
    make install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


###########################
# Module creation stage 
###########################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
    # Add lib64 to modulefile.
    Lno=$(awk "/lib/ {print NR}" ~/privatemodules/${PackName}/${PackVers})
    echo "Found lib line at line ${Lno}"
    Line=$(awk "/lib/ {print}" ~/privatemodules/${PackName}/${PackVers})
    echo "Line found is ${Line}"
    RLine=${Line//lib/lib64}
    echo "Adding line ${RLine}"
    sed -i "${Lno} i ${RLine}" ~/privatemodules/${PackName}/${PackVers}

else
    echo "Skipping Module creation stage"
fi

##########
# Finalize
##########

echo "Target successfully compiled for ${Package}"
cd ${SOURCE_ROOT}


