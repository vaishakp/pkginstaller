######################################
# Set env
######################################

PackName="amd-fftw-gnu"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${Package}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"


DOWNLOAD=yes
UNPACK=no
COMPILE=yes
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

########################################
# Download stage
########################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $File
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    #base_url="https://github.com/HDFGroup/hdf5/archive/refs/tags/"
    full_url="git@github.com:amd/amd-fftw.git"
    echo $full_url
    git clone ${full_url} ${PackName} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

#######################################
# Fetch
#######################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Fetching repo"
else
    echo "Skipping Unpack stage"
fi

cd ${PackName}
git pull --rebase
git checkout ${PackVers}
echo ${pwd}

##########################################
# Compile
##########################################

if [ $COMPILE == $"yes" ]; then
    ./configure --enable-shared --enable-openmp --enable-mpi --enable-long-double --enable-amd-opt --enable-amd-mpifft --prefix=${SOFT_ROOT}/${Package} LDFLAGS="-no-pie" 2>&1 | tee ${LogPath}
    make 2>&1 | tee ${LogPath}
    make install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


#########################################
# Module
#########################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
    mkdir -p $HOME/private_modules/amd/${PackName}
    mv $HOME/private_modules/${PackName}/${PackVers} $HOME/private_modules/amd/${PackName}/${PackVers} 
else
    echo "Skipping Module creation stage"
fi

########################
# Finalize
########################

echo "Target script finished for ${Package}"
cd ${SOURCE_ROOT}
