#!/bin/bash

SOFT_ROOT="/mnt/pfs/vaishak.p/soft"
SOURCE_ROOT="${SOFT_ROOT}/source"
PKGCOM_HOME="$(pwd)"
echo $PKGCOM_HOME

mkdir -p ${SOURCE_ROOT}

# PACKAGES
declare -A packages
packages["gcc"]=11.1.0
packages+=( ["texinfo"]=7.0.2 ["openmpi"]=4.1.4 ["hwloc"]=2.9.0 ["lapack"]=3.11.0 ["fftw"]=3.3.10 ["hdf5"]=1.14.0 ["petsc"]=3.18.3 ["gsl"]=2.7.1 ["make"]=4.4 ["papi"]=7.0.0 )


# Begin compile 
cd ${SOURCE_ROOT}
mkdir -p logs


################################################################################################################################################################################
# Packages
################################################################################################################################################################################

###########
# texinfo
###########


# Download
PackName="texinfo"
Package="${PackName}-${packages["texinfo"]}"
echo ${Package}
File="${Package}.tar.xz"
LogFile="${PackName}-${packages[${PackName}]}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=yes
UNPACK=yes
COMPILE=yes
CREATE_MODULE=yes

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $"${Package}.tar.xz"
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://ftp.gnu.org/gnu/${PackName}/"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    tar -xvf ${File} 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ${Package}
echo ${pwd}

if [ $COMPILE == $"yes" ]; then
    ./configure CFLAGS="-O3 -march=native -mtune=native -mavx2 -mfma" --prefix=${SOFT_ROOT}/${Package} 2>&1 | tee ${LogPath}
    make -j 2>&1 | tee ${LogPath}
    make install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    mkdir -p ${HOME}/privatemodules/${PackName}
    sed "s/unknown/${PackName}/g" ${PKGCOM_HOME}/unknown > ${HOME}/privatemodules/${PackName}/${packages[${PackName}]}
else
    echo "Skipping Module creation stage"
fi

echo "Target successfully compiled for ${Package}"
cd ${SOURCE_ROOT}

##################
# hwloc
#################


