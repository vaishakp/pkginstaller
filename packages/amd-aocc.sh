##############################################
# Set env
##############################################

PackName="aocc-compiler"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${PackName}-${PackVers}.tar"
LogFile="${PackName}-${PackVers}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"

DOWNLOAD=no
UNPACK=no
COMPILE=no
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

#module load gcc/11.1.0
#module load mpi/openmpi-4.1.4-gnu

####################################################
# Download
####################################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm ${File}
    rm -rf ${Package}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    base_url="https://download.amd.com/developer/eula/aocc-compiler"
    full_url="${base_url}/${File}"
    echo $full_url
    wget ${full_url} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

###################################################
# Unpack
###################################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Unpacking"
    tar -xvf ${File} -C $SOFT_ROOT 2>&1 | tee ${LogPath}
else
    echo "Skipping Unpack stage"
fi

cd ../${Package}
echo ${pwd}

####################################################
# Compile
####################################################

if [ $COMPILE == $"yes" ]; then
    bash install.sh
else
    echo "Skipping Configure stage"
fi

######################################################
# Module
######################################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    #create_module "$PackName" "$PackVers" "$Package"
    mkdir -p ~/privatemodules/amd/aocc-compiler
    #cp $PKGCOM_HOME/module_templates/aocc-compiler $HOME/privatemodules/aocc-compiler/${PackVers}
    #echo "${SOFT_ROOT}/${Package}/setenv_AOCC.sh" > $HOME/privatemodules/amd/${Package}
    sed "s/pkgvers/${PackVers}/g" ${PKGCOM_HOME}/module_templates/aocc-compiler > ${HOME}/privatemodules/amd/aocc-compiler/${PackVers}

else
    echo "Skipping Module creation stage"
fi

#####################################################
# Finalize
#####################################################

echo "Target successfully compiled for ${Package}"
cd ${SOURCE_ROOT}
