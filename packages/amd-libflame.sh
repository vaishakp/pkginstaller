######################################
# Set env
######################################

PackName="amd-libflame"
PackVers="${packages[${PackName}]}"
Package="${PackName}-${PackVers}"

echo ${Package}

File="${Package}.tar.gz"
LogFile="${Package}.log"
LogPath="${SOURCE_ROOT}/logs/$LogFile"


DOWNLOAD=no
UNPACK=no
COMPILE=no
CREATE_MODULE=yes

PKG_CFLAGS=$CFLAGS
PKG_CXXFLAGS=$CFLAGS

########################################
# Download stage
########################################

if [ ${DOWNLOAD} == $"yes" ]; then
    rm $File
    rm -rf ${PackName}
else 
    echo "Skipping Download stage"
fi


if [ -f "$File" ]; then
    echo "$File exists."
else 
    echo "$File does not exist. Initiating download"
    #base_url="https://github.com/HDFGroup/hdf5/archive/refs/tags/"
    full_url="git@github.com:amd/libflame.git"
    echo $full_url
    git clone ${full_url} ${PackName} 2>&1 | tee ${LogPath}
    echo "File downloaded"
fi

#######################################
# Fetch
#######################################

if [ $UNPACK == $"yes" ]; then 
    # Unpack
    echo "Fetching repo"
else
    echo "Skipping Unpack stage"
fi

cd ${PackName}
git pull --rebase
git checkout ${PackVers}
echo ${pwd}

##########################################
# Compile
##########################################

if [ $COMPILE == $"yes" ]; then
    ./configure --enable-lapack2flame --enable-external-lapack-interfaces --enable-dynamic-build --enable-max-arg-list-hack --prefix=${SOFT_ROOT}/${Package} \
		CC=clang CXX=clang++ F77=flang 2>&1 | tee ${LogPath}
    make 2>&1 | tee ${LogPath}
    make install 2>&1 | tee ${LogPath}
else
    echo "Skipping Configure stage"
fi


#########################################
# Module
#########################################

if [ $CREATE_MODULE == $"yes" ]; then
    # Create ModuleFile
    echo "Creating module file"
    create_module "$PackName" "$PackVers" "$Package"
    mkdir -p $HOME/privatemodules/amd/${PackName}
    mv $HOME/privatemodules/${PackName}/${PackVers} $HOME/privatemodules/amd/${PackName}/${PackVers}
else
    echo "Skipping Module creation stage"
fi

########################
# Finalize
########################

echo "Target script finished for ${Package}"
cd ${SOURCE_ROOT}
