#!/bin/bash

###################################################################################
# pkgcompile version 1. 
# Authors: Vaishak Prasad
# email: vaishakprasad@gmail.com
# Date: 20 Feb 2023

###################################################################################
# Functions
###################################################################################

create_module () {
    # A bash function to create a module file from the template.
    # PackName is 1
    # PackVers is 2
    # Package is 3
    mkdir -p ${HOME}/privatemodules/$1
    sed "s/unknown/${1//-/_}/g" ${PKGCOM_HOME}/module_templates/unknown > ${HOME}/privatemodules/$1/$2
    sed -i "s/pkgvers/$3/g" ${HOME}/privatemodules/$1/$2
}



####################################################################################
# Set PATHS
####################################################################################

SOFT_ROOT="/mnt/pfs/vaishak.p/soft"
SOURCE_ROOT="${SOFT_ROOT}/source"
PKGCOM_HOME="$(pwd)"
echo $PKGCOM_HOME
mkdir -p ${SOURCE_ROOT}

# PACKAGES
declare -A packages
packages["gcc"]=11.1.0
packages+=( ["cmake"]=3.25.2 ["texinfo"]=7.0.2 ["openmpi"]=4.1.4 ["hwloc"]=2.9.0 ["lapack"]=3.11.0 ["fftw"]=3.3.10 ["hdf5"]=1.14.0 ["petsc"]=3.18.4 ["gsl"]=2.7.1 ["make"]=4.4 ["papi"]=7.0.0 ["knem"]=1.1.4 ["xpmem"]=2.6.5 ["aocc-compiler"]=4.0.0 ["amd-fftw"]=4.0 ["amd-libmem"]=4.0 ["amd-libm"]=4.0 ["amd-libflame"]=4.0 ["amd-blis"]=4.0 ["openblas"]=0.3.21 )

# Note: znver2 arch type is not present in older gcc ~<9 and native should be used.
CFLAGS="-O3 -march=native -mtune=native -mavx2 -mfma -fPIC -fPIE"
CPPFLAGS=$CFLAGS

# Use the above flags even if not 
# specified in the package.
export CFLAGS=${CFLAGS}
export CXXFLAGS=${CXXFLAGS}

# Begin compile 
cd "${SOURCE_ROOT}"
mkdir -p logs

module purge
module load use.own
#conda deactivate

#######################################################################################
# Packages
#######################################################################################


##########
# Texinfo
##########
#source $PKGCOM_HOME/packages/texinfo.sh
module load texinfo/${packages["texinfo"]}
echo "Using $(which makeinfo)"
sleep 1

#######
# Make
#######
#source $PKGCOM_HOME/packages/make.sh
module load make/${package["make"]}
echo "Using $(which make)"
sleep 1

######
# gcc
######
#module load gcc/12.2.0
source $PKGCOM_HOME/packages/gcc.sh
module load gcc/${packages["gcc"]}
# Note: Please check the modulefile 
# if lib64 has been added to LDL path.
echo "Using $(which gcc)"
sleep 1

########
# hwloc
########
#source $PKGCOM_HOME/packages/hwloc.sh
module load hwloc/${packages["hwloc"]}


##############
# openGL hack
##############

#source $PKGCOM_HOME/packages/openGL.sh
module load openGL/0
echo "Using $(which hwloc-bind)"
sleep 1

########
# cmake
########
#module unload gcc/11.1.0
#module load gcc/12.2.0
#source $PKGCOM_HOME/packages/cmake.sh
module load cmake/${package["cmake"]}
echo "Using $(which cmake)"
sleep 1

#######
# kenm
#######
#source $PKGCOM_HOME/packages/knem.sh
module load knem/${package["knem"]}
#echo "Using $(which cmake)"
sleep 1

########
# xpmem
########
#source $PKGCOM_HOME/packages/xpmem.sh
module load xpmem/${package["xpmem"]}
#echo "Using $(which cmake)"
sleep 1

##########
# openmpi
##########
#module load gcc/12.2.0
#source $PKGCOM_HOME/packages/openmpi.sh
module load mpi/"openmpi-${packages["openmpi"]}-gnu"
echo "Using $(which mpicc)"


##########
# openmpi-aocc
##########
#module load amd/aocc-compiler/4.0.0
#source $PKGCOM_HOME/packages/openmpi-aocc.sh
#module load mpi/"openmpi-${packages["openmpi"]}-gnu"
#echo "Using $(which cmake)"


#########
# lapack
#########
#source $PKGCOM_HOME/packages/lapack_make.sh
module load lapack/${packages["lapack"]}

###########
# Openblas
###########
#source $PKGCOM_HOME/packages/openblas.sh
module load openblas/${packages["openblas"]}


#######
# fftw
#######
#source $PKGCOM_HOME/packages/fftw.sh
module load fftw/${packages["fftw"]}
echo "Using $(which fftw-wisdom)"

######
# gsl
######
#source $PKGCOM_HOME/packages/gsl.sh
module load gsl/${packages["gsl"]}
#echo "Using $(which cmake)"

#######
# hdf5
#######
source $PKGCOM_HOME/packages/hdf5_git.sh
module load "hdf5/${packages["hdf5"]}-serial"
echo "Using $(which h5cc)"

#######
# hdf5
#######
source $PKGCOM_HOME/packages/hdf5_parallel_git.sh
module load "hdf5/${packages["hdf5"]}-parallel"
echo "Using $(which h5cc)"

#######
# papi
#######
#source $PKGCOM_HOME/packages/papi.sh
module load papi/${packages["papi"]}
echo "Using $(which papi_version)"

########
# petsc
########
#source $PKGCOM_HOME/packages/petsc.sh
#module load petsc/${packages["petsc"]}-nh5
#echo "Using $(which cmake)"

###########
# amd aocc
###########

#source $PKGCOM_HOME/packages/amd-aocc.sh
module load amd/aocc-compiler/${packages["aocc-compiler"]}

###########
# amd aocl
###########

#source $PKGCOM_HOME/packages/amd-blis.sh
module load amd/amd-blis/${packages["amd-blis"]}

#source $PKGCOM_HOME/packages/amd-libflame.sh
module load amd/amd-libflame/${packages["amd-libflame"]}

#source $PKGCOM_HOME/packages/amd-fftw.sh
module load amd/amd-fftw/${packages["amd-fftw"]}

#source $PKGCOM_HOME/packages/amd-libm.sh
module load amd/amd-libm/${packages["amd-libm"]}

#source $PKGCOM_HOME/packages/amd-libmem.sh
module load amd/amd-libmem/${packages["amd-libmem"]}
