[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/vaishakp/pkginstaller/commits/main/LICENSE)

pkginstaller
=============

A very simple bash script to download and install essential packages.

This is a semi-automated script that, for predefined list of packages, :

* Downloads the source code as tar / git.
* Compile it using preferred compiler and optimization flags.
* Create a module file.


## How to use

The master script is "StartCompile.sh". For each package that needs to be installed, the user needs to 

* list its details (name, version) in the main script.
* create a package specific helper script as "package-name.sh" and source it in the main script. This file may be modelled after exisiting package scripts. 
* set the env variable SOFT\_ROOT. This is the root directory where all the binaries will be compiled to.
* runs the main script using bash.

In creating the helper script, one has to/ can:

* Provide package specific CFLAGS, CXXFLAGS, FCFLAGS, apart from the default defined in the master script.
* Add mirrors for the source code. This is done manually.
* Edit/ set the compile commands.

Each package is installs in four steps:

* Download source
* Unpack source
* Compile
* Create modulefile

and the user can toggle the flags for turninig on/off each of these steps in the helper files.

This script can be used to install scripts using make, cmake or just extraction (as with binaries). It is recommended to preserve the order of the packages as present in the main script.

The install stdout and stderr are loged in 
```sh
$SOFT\_ROOT/logs
``` 
directory.
### Module files

The module files are created in 
```sh
$HOME/privatemodules
```
path, under the "library name / version" format.

## Prerequisites

* Environment modules.
* A C compiler
* git

## List of default packages available

* texinfo
* make
* gcc
* hwloc
* cmake
* knem
* xpmem
* openmpi
* netlib-lapack
* fftw
* gsl
* hwloc
* hdf5
* papi
* petsc
* amd libraries
	* aocc-compilers
	* amd-blis
	* amd-libflame
	* amd-fftw
	* amd-libm
	* amd-libmem
